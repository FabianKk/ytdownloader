import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import ytdl from 'ytdl-core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  link : string;
  t : any;
  constructor(public navCtrl: NavController) {
    this.link = "https://www.youtube.com/watch?v=ZubukRzc0Ac";
  }
  public downloadClick()
  {
    if(this.t === undefined)
    {
      this.t = ytdl(this.link);
      console.log(this.t );
    }
    else
    {
      console.log(this.t);
    }
    
    
  }
}
